<?php
//セッションの開始
session_start();

// echo'<pre>';
// print_r($_SESSION);
// echo'</pre>';
require_once("util.php");
$name = '';
$kotoba = '';

if(!empty($_SESSION['name'])){
	$name = $_SESSION['name'];
}
//POSTされた詳細をセッション変数に代入
if(!empty($_SESSION['shousai'])){
	$kotoba = $_SESSION['shousai'];
}

//POSTされたオーダーをセッション変数に代入
if(!empty($_SESSION['order'])){
	$order = $_SESSION['order'];
}
    //エラーを入れる配列
    $error = [];
    if (isSet($_POST["order"])){
        //オーダーかどうか確認する
        $orders = ["リース","スワッグ","キャンドル","その他・質問のみ"];
        //$ordersに含まれていない値があれば取り出す
        $diffValue = array_diff($_POST["order"], $orders);
        //規定外の値が含まれていなければOK
        if(count($diffValue)==0){
            //チェックされている値を取り出す
            $orderChecked = $_POST["order"];
        } else {
            $orderChecked = [];
            $error[] = "「オーダー」の入力中にエラーがありました。";
        }
        } else {
            //POSTされた値がないとき
            $orderChecked = [];
        }

        //初期値でチェックするかどうか
        function checked($value, $question){
            if (is_array($question)){
                //配列の時、値が含まれていればtrue
                $isChecked = in_array($value, $question);
            } else {
              //配列でないとき値が一致すればtrue
                $isChecked = ($value===$question);  
            }
           
            if($isChecked){
                //チェックする
                echo "checked";
            } else {
                echo "";
            }
        }

?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>お問い合わせフォーム</title>
		<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/form.css">
	<link href="https://fonts.googleapis.com/css?family=Parisienne|Yellowtail" rel="stylesheet">
</head>
<body>
		<header>
		<div id="menu">
			<a class="button" href="index.html">Top</a>
			<a class="button" href="sub.html">About</a>
			<a class="button" href="wreath.html">Wreath & Swag</a>
			<a class="button" href="candle.html">Candle & Wax</a>
			<a class="button" href="form.html">Contact</a>
		</div>
		<h1>- Contact -</h1>
	</header>
<div>
	<form method="POST" action="confirm.php">
		<ul>
		<li><span>オーダー：</span>
                <label><input type="checkbox" name="order[]" value="リース" <?php checked("リース", $orderChecked); ?> >リース</label>
                <label><input type="checkbox" name="order[]" value="スワッグ" <?php checked("スワッグ", $orderChecked); ?> >スワッグ</label>
                <label><input type="checkbox" name="order[]" value="キャンドル" <?php checked("キャンドル", $orderChecked); ?> >キャンドル</label>
                <label><input type="checkbox" name="order[]" value="その他・質問のみ" <?php checked("その他・質問のみ", $orderChecked); ?> > その他・質問のみ</label>
            </li>
			<li><p>
					<label>
						名前：
						<input type="text" name="name" size="30" placeholder="なまえ" value="<?php echo es($name) ?>">
					</label>
				</p></li>
			<li><p>
				<label>
						詳細：
						<textarea id="shousai" name="shousai" cols="33" rows="4" maxlength="20" placeholder="ご希望のスタイル、花材などご記入ください" value="<?php echo es($shousai) ?>"></textarea>
				</label>
				</p>
			</label></li>
			<li><input type="submit" value="確認する"></li>
		</ul>
	</form>
</div>
</body>
</html>
