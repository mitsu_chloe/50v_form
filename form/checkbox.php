<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>checkbox</title>
</head>
<body>
<div>
    <?php
    require_once("util.php");
    //文字コード検証
    if(!cken($_POST)){
        $encoding = mb_internal_encoding();
        $err = "Encoding Error! The expected encording is " . $encoding ;
        //エラーメッセージを出してキャンセル
        exit($err);
    }
    //HTMLエスケープ(XSS対策）

    //エラーを入れる配列
    $error = [];
    if (isSet($_POST["order"])){
        //オーダーかどうか確認する
        $orders = ["リース","スワッグ","キャンドル","その他・質問のみ"];
        //$ordersに含まれていない値があれば取り出す
        $diffValue = array_diff($_POST["order"], $orders);
        //規定外の値が含まれていなければOK
        if(count($diffValue)==0){
            //チェックされている値を取り出す
            $orderChecked = $_POST["order"];
        } else {
            $orderChecked = [];
            $error[] = "「オーダー」の入力中にエラーがありました。";
        }
        } else {
            //POSTされた値がないとき
            $orderChecked = [];
        }

        //初期値でチェックするかどうか
        function checked($value, $question){
            if (is_array($question)){
                //配列の時、値が含まれていればtrue
                $isChecked = in_array($value, $question);
            } else {
              //配列でないとき値が一致すればtrue
                $isChecked = ($value===$question);  
            }
           
            if($isChecked){
                //チェックする
                echo "checked";
            } else {
                echo "";
            }
        }
        ?>

        <!-- 入力フォーム -->
        <form method="POST" action="<?php echo es($_SERVER['PHP_SELF']); ?>">
        <ul>
            <li><span>オーダー：</span>
                <label><input type="checkbox" name="order[]" value="リース" <?php checked("リース", $orderChecked); ?> >リース</label>
                <label><input type="checkbox" name="order[]" value="スワッグ" <?php checked("スワッグ", $orderChecked); ?> >スワッグ</label>
                <label><input type="checkbox" name="order[]" value="キャンドル" <?php checked("キャンドル", $orderChecked); ?> >キャンドル</label>
                <label><input type="checkbox" name="order[]" value="その他・質問のみ" <?php checked("その他・質問のみ", $orderChecked); ?> > その他・質問のみ</label>
            </li>
            <li><input type="submit" value="送信する"></li>
        </ul>
        </form>

        <?php
        //オーダーがチェックされていれば結果を表示する
        $isSelected = count($orderChecked) > 0;
        if($isSelected){
            echo "<HR>";
            print_r($orderChecked);
        } else{
            echo"<HR>";
            echo"選択されているものはありません。";
        }
        //エラー表示
        if(count($error) > 0){
            echo"<HR>";
            //値を"<br>"で連結して表示する
            echo'<span class="error">', implode("<br>", $error),'</span>';
        }
        ?>
</div>
</body>
</html>