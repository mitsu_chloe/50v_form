<?php
//セッションの開始
session_start();
require_once("util.php");
?>

<?php
//文字エンコードの検証
if (!cken($_POST)){
	$encoding = mb_internal_encording();
	$err = "Encoding Error! The expected encoding is " . $encoding ;
	//エラーメッセージを出して、以下のコードをすべてキャンセルする
	exit($err);
}
?>

<?php
//POSTされた値をセッション変数に受け渡す
if (isset($_POST['name'])){
	$_SESSION['name'] = $_POST['name'];
}

if (isset($_POST['shousai'])){
	$_SESSION['shousai'] = $_POST['shousai'];
}
//入力データの取り出しとチェック
$error = [];
//名前
if (empty($_SESSION['name'])){
	//　未設定の時エラー
	$error[] = "名前を入力してください。";
} else {
	//名前を取り出す
	$name = trim($_SESSION['name']);
}

//好きな言葉
if (empty($_SESSION['shousai'])){
	$error = "ご希望のスタイル、花材などご記入ください。";
} else {
	//詳細を取り出す
	$shousai = trim($_SESSION['shousai']);
}
?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>確認ページ</title>
	<link href="css/form.css" rel="stylesheet">
</head>
<body>
	<div>
		<form>
			<?php if (count($error)>0){ ?>
				<!-- エラーがあったとき -->
				<span class="error><?php echo implode('<br>', $error); ?></span><br>
				<span>
					<input type="button" value="戻る" onclick="location.href='input.php'">
				</span>
			<?php } else { ?>
				<!--エラーがなかった時 -->
				<span>
					名前　　  ：<?php echo es($name); ?><br>
					詳細　　  ：<?php echo es($shousai); ?><br>
					<input type="button" value="戻る" onclick="location.href='input.php'">
					<input type="button" value="送信する" onclick="location.href='thankyou.php'">
				</span>
				<?php } ?>
			</form>
		</div>
	</body>
	</html>