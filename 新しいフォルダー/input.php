<?php
//セッションの開始
session_start();

// echo'<pre>';
// print_r($_SESSION);
// echo'</pre>';
require_once("util.php");
$name = '';
$kotoba = '';

if(!empty($_SESSION['name'])){
	$name = $_SESSION['name'];
}

if(!empty($_SESSION['shousai'])){
	$kotoba = $_SESSION['shousai'];
}

?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>お問い合わせフォーム</title>
	<link href="../css/form.css rel="stylesheet">
</head>
<body>
<div>
	<form method="POST" action="confirm.php">
		<ul>
			<li><p>
					<label>
						名前：
						<input type="text" name="name" size="30" placeholder="なまえ" value="<?php echo es($name) ?>">
					</label>
				</p></li>
			<li><p>
				<label>
						詳細：
						<textarea id="shousai" name="shousai" cols="33" rows="4" maxlength="20" placeholder="ご希望のスタイル、花材などご記入ください" value="<?php echo es($shousai) ?>"></textarea>
				</label>
				</p>
			</label></li>
			<li><input type="submit" value="確認する"></li>
		</ul>
	</form>
</div>
</body>
</html>
