	<?php
	//セッションの開始
	session_start();
	require_once("util.php");
	//文字エンコードの検証
	if (!cken($_POST)){
		$encoding = mb_internal_encoding();
		$err = "Encording Error! The expected encording is " . $encording ;
		//エラーメッセージを出して、以下のコードをすべてキャンセルする
		exit($err);
	}
	//HTMLエスケープ(XSS対策)
	$_POST = es($_POST);

	//確認ページから戻ってきたとき、セッション変数の値を取り出す
	if (empty($_SESSION['order'])){
		$order = "";
	} else {
		$order = $_SESSION['order'];
	}

	if (empty($_SESSION['pref_id'])){
		$pref_id ="";
	} else {
		$pred_id = $_SESSION['pref_id'];
	}

	if (empty($_SESSION['address'])){
		$address = "";
	} else {
		$address = $_SESSION['address'];
	}

	if (empty($_SESSION['name'])){
		$name = "";
	} else {
		$name = $_SESSION['name'];
	}

		if (empty($_SESSION['shousai'])){
		$shousai = "";
	} else {
		$shousai = $_SESSION['shousai'];
	}
	?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>お問い合わせフォーム</title>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>


	<?php
	//エラーを入れる配列
	$error = [];
	if (isSet($_POST["order"])){
		//オーダーがどうか確認する
		$orders = ["リース", "スワッグ", "キャンドル", "ワックス"];
		//オーダーに含まれていない値があれば取り出す
		$diffValue = array_diff($_POST["order"], $orders);
		//規定外の値が含まれていなければOK
		if (count($diffValue)==0){
			//チェックされている値を取り出す
			$orderChecked = $_POST["order"];
		} else {
			$orderChecked = [];
			$error[] = "「オーダー」に入力エラーがありました。";
		} 
	} else {
			//POSTされた値がないとき
			$orderChecked = [];
		}

	//プルダウン都道府県
	//POSTされた都道府県を取り出す
	if (isSet($_POST["pref_id"])){
		$i = $pref_idValues;
		for ($i=0; $i < 48; $i++) { 
			$pref_idValues = [$i];
		}
		$isPref_id = in_array($_POST["pref_id"], $pref_idValues);
		//$pref_idValuesに含まれている値ならばOK
		if ($isPref_id){
			//都道府県ならば処理とフォーム表示の値で使う
			$pref_id = $_POST["pref_id"];
		} else {
			$pref_id = "error";
			$error[] = "都道府県入力エラーがありました";
		}
	} else {
		//POSTされた値がないとき
		$isPref_id =false;
	}

	//POSTされた住所を取り出す
	if (isSet($_POST["address"])){
		$address = $_POST["address"];
		//HTMLタグやPHPタグを削除する
		$address = strip_tags($address);
		//HTMLエスケープ
		$address = es($address);
	} else {
		//POSTされた値がないとき
		$address = "";
	}


	//POSTされたテキスト分を取り出す
	if (isSet($_POST["name"])){
		$name = $_POST["name"];
		//HTMLタグやPHPタグを削除する
		$name = strip_tags($name);
		//HTMLエスケープ
		$name = es($name);
	} else {
		//POSTされた値がないとき
		$name = "";
	}

		//POSTされたテキスト分を取り出す
	if (isSet($_POST["shousai"])){
		$shousai = $_POST["shousai"];
		//HTMLタグやPHPタグを削除する
		$shousai = strip_tags($shousai);
		//HTMLエスケープ
		$shousai = es($shousai);
	} else {
		//POSTされた値がないとき
		$shousai = "";
	}



	//初期値でチェックするかどうか
	function checked($value, $question){
		if (is_array($question)){
			//配列の時、値が含まれていれば、true
			$isChecked = in_array($value, $question);
		} else {
			//配列ではないとき値が一致すればtrue
			$isChecked = ($value===$question);
		}
		if ($isChecked){
			//チェックする
			echo "checked";
		} else {
			echo "";
		}
	}



	?>
<!-- 入力フォーム -->
		<h2>オーダーフォーム</h2>
		<div>
			<form action="confirm.php" method="post">
<!-- 				<p>
					<label for="wreath">
						<input type="checkbox" id="wreath" name="order[]" value="wreath">リース
					</label>
					<label for="swag">
						<input type="checkbox" id="swag" name="order[]" value="swag">スワッグ
					</label>
					<label for="candle">
						<input type="checkbox" id="candle" name="order[]" value="candle">キャンドル
					</label>
					<label for="wax">
						<input type="checkbox" id="wax" name="order[]" value="wax">ワックス
					</label>
				</p> -->
				<p>
					<label>
						住所：
						<select name="pref_id">
							<option value="" selected>都道府県</option>
							<option value="1">北海道</option>
							<option value="2">青森県</option>
							<option value="3">岩手県</option>
							<option value="4">宮城県</option>
							<option value="5">秋田県</option>
							<option value="6">山形県</option>
							<option value="7">福島県</option>
							<option value="8">茨城県</option>
							<option value="9">栃木県</option>
							<option value="10">群馬県</option>
							<option value="11">埼玉県</option>
							<option value="12">千葉県</option>
							<option value="13">東京都</option>
							<option value="14">神奈川県</option>
							<option value="15">新潟県</option>
							<option value="16">富山県</option>
							<option value="17">石川県</option>
							<option value="18">福井県</option>
							<option value="19">山梨県</option>
							<option value="20">長野県</option>
							<option value="21">岐阜県</option>
							<option value="22">静岡県</option>
							<option value="23">愛知県</option>
							<option value="24">三重県</option>
							<option value="25">滋賀県</option>
							<option value="26">京都府</option>
							<option value="27">大阪府</option>
							<option value="28">兵庫県</option>
							<option value="29">奈良県</option>
							<option value="30">和歌山県</option>
							<option value="31">鳥取県</option>
							<option value="32">島根県</option>
							<option value="33">岡山県</option>
							<option value="34">広島県</option>
							<option value="35">山口県</option>
							<option value="36">徳島県</option>
							<option value="37">香川県</option>
							<option value="38">愛媛県</option>
							<option value="39">高知県</option>
							<option value="40">福岡県</option>
							<option value="41">佐賀県</option>
							<option value="42">長崎県</option>
							<option value="43">熊本県</option>
							<option value="44">大分県</option>
							<option value="45">宮崎県</option>
							<option value="46">鹿児島県</option>
							<option value="47">沖縄県</option>
						</select>
					</label>
				</p>
				<p>
					<label>住所：
						<input type="text" name="address" size="30" maxlengh="20" placeholder="住所" value="<?php echo es($address) ?>">
					</label>
				</p>
				<p>
					<label>
						名前：
						<input type="text" name="name" size="30" value="<?php echo es($name) ?>">
					</label>
				</p>
				<p>
				<label>
						詳細：
						<textarea id="shousai" name="shousai" cols="33" rows="4" maxlength="20" placeholder="ご希望のスタイル、花材などご記入ください" value="<?php echo es($shousai) ?>"></textarea>
				</label>
				</p>
				<p>
					<input type="submit" value="送信">
				</p>
			</div>