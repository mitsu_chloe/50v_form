<?php
//セッションの開始
session_start();
require_once("util.php");
// echo '<pre>';
// print_r($_SESSION);
// echo '</pre>';
$error = [];
//セッションのチェック
if (!empty($_SESSION['name']) && !empty($_SESSION['shousai'])){
//セッション変数から値を取り出す
	$name = $_SESSION['name'];
	$shousai = $_SESSION['shousai'];
} else {
	$error[] = "セッションエラーです。";
}
//HTMLを表示する前にセッションを破棄する
killSession();

//セッションを破棄する
function killSession(){
	//セッション変数の値を空にする
	$_SESSION = [];
	//セッションクッキーを破棄する
	if (isset($_COOKIE[session_name()])){
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time()-36000, $params['path']);
	}
	//セッションを破棄する
	session_destroy();
}
?>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>完了ページ</title>
</head>
<body>
<div>
	<?php if (count($error)>0){ ?>
		<!--　エラーがあったとき　-->
		<span class="error"><?php echo implode('<br>', $error); ?></span><br>
		<a href="input.php"最初のページに戻る</a>
	<?php } else { ?>
		<!-- エラーがなかったとき -->
		<span>
			次のように受け付けました。ありがとうございました。
			<hr>
			<span>
				名前：<?php echo es($name); ?><br>
				詳細：<?php echo es($shousai); ?><br>
	<a href="input.php">最初のページに戻る</a>
</span>
<?php } ?>
</div>
</body>
</html>